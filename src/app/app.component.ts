import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TicTacToe';
  winners:any=[];
  selectPlayer:any;
  seleect_combinationX:any=[];
  seleect_combinationO:any=[];
  constructor() { 
   
    this.winners=[
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ]
  }

  move(score:any){
    if(this.selectPlayer == undefined || this.selectPlayer == null || this.selectPlayer ==''){
     alert('Please select player');
    }
    else{
      if(this.selectPlayer == 'X'){
        this.seleect_combinationX.push(score)
      }
      if(this.selectPlayer == 'O'){
        this.seleect_combinationO.push(score)
      }
    }
  }

  slectPlayer(player:any){
    this.selectPlayer= player;
  }
}
